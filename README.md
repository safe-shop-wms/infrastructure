# Infrastructure

This repository contains the infrastructure as code that defines the backend services for the web portal and the STAC device.

## Contents

### `templates`: CloudFormation Templates
The IaC (Infrastructure as Code) is defined in these templates. There is currently a single template for one environment.
The exception to this is the bucket for the web portal. There is a 'test' and 'prod' bucket defined in the template.

To deploy and update the template, utilize the CloudFormation service in the AWS console. See the [Infrastructure Setup](https://gitlab.com/safe-shop-wms/product-backlog/-/wikis/Setup-Guide/Infrastructure-Setup) wiki for instructions on deploying the stack for the first time.

When updating the stack, use the 'Update' button on the stack page and edit the template in the designer. Then hit the 'Create Stack' from designer view and it will prompt you to review your changes and deploy them.

Note: When the CloudFormation template updates are deployed, updates to the API are applied, but not deployed. You have to manually deploy the updates.
Whenever you make API changes, you'll need to go to the `SafeShop-Prod` api in the API Gateway service on the AWS console.
On the resources tab 'Actions' dropdown, select 'Deploy API'. Select the 'prod' stage and hit 'Deploy'.

### `requests`: HTTP Request Samples
These HTTP requests <api_name>-<env>.http correspond with the APIs setup for the specified environment in AWS.
These are formatted to be used with the REST Client extension for VS Code.

Note: All endpoints now require some form of authorization and cannot be tested as is in VSCode. A method of testing them outside of the app is TBD.

### `database`: Database Item Examples
These <table_name>.json files include the json used to create new items for the databases.
They are for convenience when manually adding items via the DynamoDB GUI for testing purposes.

### `functions`: Lambda Function Code
This directory contains a subdirectory for each API resource which utilizes Lambda functions. The subdirectory within the resource directory is named for the lambda function utilized by that resource.

The lambda function code (`.js` files) is currently placed within the IaC templates. Later, these files will be extracted back into individual `.js` files for easier maintability.

The lambda function directory also contain `tests` subdirectories with the JSON utilized to test the lambda functions via the GUI.
These could potentially be automated later somehow.

## References

- [AWS Cloud Formation: AWS Resource and Property Types Reference](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html)